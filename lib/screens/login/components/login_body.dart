import 'package:flutter/material.dart';
import 'package:kec/components/default_button.dart';

import '../../../constants.dart';

class LoginBody extends StatelessWidget {
  const LoginBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SizedBox(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    "Login",
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                Text(
                  "Please enter your mobile no",
                  textAlign: TextAlign.right,
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Container(
                      width: 60,
                      child: TextFormField(
                        enabled: false,
                        initialValue: "+91",
                        decoration: InputDecoration(
                            enabledBorder: outlineInputBorder(),
                            disabledBorder: outlineInputBorder()),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 130,
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: "Enter your mobile no",
                          labelText: "Mobile No",
                          enabledBorder: outlineInputBorder(),
                          focusedBorder: outlineInputBorder(),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                DefaultButton(name: "Continue", press: () {}),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
