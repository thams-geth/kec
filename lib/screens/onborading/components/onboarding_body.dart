import 'package:flutter/material.dart';
import 'package:kec/components/default_button.dart';

import '../../../constants.dart';
import '../../../routes.dart';
import 'onboarding_content.dart';

class OnBoardingBody extends StatefulWidget {
  const OnBoardingBody({
    Key? key,
  }) : super(key: key);

  @override
  _OnBoardingBodyState createState() => _OnBoardingBodyState();
}

class _OnBoardingBodyState extends State<OnBoardingBody> {
  int currentPage = 0;
  List<Map<String, dynamic>> onBoardingItems = [
    {
      "title": "Post what you are thinking",
      "image": "assets/images/kec_logo.png"
    },
    {
      "title": "Connect & Chat with people",
      "image": "assets/images/kec_logo.png"
    },
    {"title": "Share & Attend event", "image": "assets/images/kec_logo.png"},
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SizedBox(
          width: double.infinity,
          child: Column(
            children: [
              Expanded(
                  flex: 3,
                  child: PageView.builder(
                      onPageChanged: (value) {
                        setState(() {
                          currentPage = value;
                        });
                      },
                      itemCount: onBoardingItems.length,
                      itemBuilder: (context, index) => OnBoardingContent(
                          title: onBoardingItems[index]["title"],
                          image: onBoardingItems[index]["image"]))),
              Expanded(
                flex: 1,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ...List.generate(
                          onBoardingItems.length,
                          (index) => buildDot(index),
                        )
                      ],
                    ),
                    Spacer(
                      flex: 2,
                    ),
                    DefaultButton(
                        name: "Continue",
                        press: () {
                          Navigator.popAndPushNamed(
                              context, RoutesName.loginScreen);
                        }),
                    Spacer()
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  AnimatedContainer buildDot(int index) {
    return AnimatedContainer(
      margin: EdgeInsets.only(right: 6),
      duration: kAnimationDuration,
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
          color: currentPage == index ? kPrimaryColor : kSecondaryColor,
          borderRadius: BorderRadius.circular(10)),
    );
  }
}
