import 'package:flutter/material.dart';

class OnBoardingContent extends StatelessWidget {
  const OnBoardingContent({
    Key? key,
    required this.title,
    required this.image,
  }) : super(key: key);

  final String title;
  final dynamic image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Spacer(
          flex: 2,
        ),
        Image.asset(
          image,
          width: 200,
          height: 200,
        ),
        Spacer(
          flex: 2,
        ),
        Text(
          title,
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
        Spacer(),
      ],
    );
  }
}
