import 'package:flutter/material.dart';

import 'screens/login/login_screen.dart';
import 'screens/onborading/onboarding.dart';
import 'screens/splash/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  RoutesName.splashScreen: (context) => SplashScreen(),
  RoutesName.loginScreen: (context) => LoginScreen(),
  RoutesName.onBoardingScreen: (context) => OnBoardingScreen(),
};

class RoutesName {
  static final splashScreen = "/splashScreen";
  static final loginScreen = "/loginScreen";
  static final onBoardingScreen = "/onBoardingScreen";
}
